/*
 *
 * $Id: update.c,v 1.4 2010/10/16 02:28:39 rader Exp $
 *
 */

/*------------------------------------------------------------------*/

#include "cnagios.h"
#include <signal.h>

extern int need_swipe;
extern int polling_interval;

/*------------------------------------------------------------------*/

void update_display() 
{
  alarm(0);
  read_status();
  need_swipe = 1;
  draw_screen();
#ifdef SUN
  signal(SIGALRM,update_display);
#endif
  alarm(polling_interval);
}

