/*
 * $Id: debug.c,v 1.8 2006/11/28 12:33:45 rader Exp $
 */

#include <curses.h>
#include "cnagios.h"

extern int debugging;

/*------------------------------------------------------------------*/

debug(msg,a,b,c,d,e,f,g,h,i,j,k)
char *msg;
{
  if ( debugging == 1 ) {
    fprintf(stderr,msg,a,b,c,d,e,f,g,h,i,j,k);
    fprintf(stderr,"\r\n");
    fflush(stderr);
  }
}

